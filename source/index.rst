.. Unix Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis
   documentation master file, created by
   sphinx-quickstart on Wed Aug 24 11:02:21 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Unix course for SARS-CoV-2 sequence analysis
========================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   Installation/index
   Unix/index
   Good_Practices/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. only:: html

   :download:`Download the Latex version <_static/unix_course_for_sars-cov-2_sequence_analysis.pdf>`


.. container:: twocol

   .. container:: leftside

      .. image:: _static/imgs/pasteur-network.png
         :height: 80px
         :align: left
         :target: https://pasteur-network.org/

   .. container:: rightside

      .. image:: _static/imgs/institut_pasteur_logo_2020.png
         :height: 60px
         :align: right
         :target: https://www.pasteur.fr/fr