.. Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

.. role:: red

.. _Display_files:

*************
Display Files
*************

cat
===

* **cat file1 file2 file3 ...**

displays on the standard output the content of the **text** file 
in same sequence as the arguments.
 
more
====

* **more file1 ...**

more is a command to view (but not modify) the contents of a text file one screen.

* press ``return`` to go one line down.
* press ``space`` to go one page down.
* press ``b`` to go one page up.

less
====

* **less file1 ...***

less is similar to more but with more features

* ``return`` to go one line down.
* ``space`` to go one page down.
* ``b`` to go one page up.
* ``/ pattern`` to search pattern in file. 
* then ``n`` to jump to next occurence.
* ``g`` to go to the first line
* ``G`` to the last line
* ...


head
====

* **head file1** output the first part of files 
* **head -n number file1** output the **number** first lines of files

::

   $head ~bneron/DataBio/Sequences/Nucleique/qr1Large.fasta 
   >Seq0 qr1 sequence
   gtcagaccttcctcctcagaagctcacagaaaaacacgctttctgaaagattccacactcaatgccaaaatataccacag
   gaaaattttgcaaggctcacggatttccagtgcaccactggctaaccaagtaggagcacctcttctactgccatgaaagg
   aaaccttcaaaccctaccactgagccattaactaccatcctgtttaagatctgaaaaacatgaagactgtattgctcctg
   ...

it's very useful especially when the files are large.


tail
====

* **tail file1** output the last part of files 
* **tail -n number file1** output the **number** last lines of files

::

   $tail ~bneron/DataBio/Sequences/Nucleique/qr1Large.fasta 
   ...
   tgcctaattgttacaaattcattaacagcagtagtgtttaagagctctaagtagctcatacttaaagagtgtttccctct
   gcacgtaccaataatctcttagtaagacgactaacttgatgactgagttgttcacaaaacccttccgtagaattatagga
   tgtggattttataatacaccgataaaaactactttgaaataggttttcttttcctgtcgtttactgtcagtagctctctg
   catagaaatgtcaaataaacagatcttgttttggtttc

it's very useful especially when the files are large.


file
====

If you do not know the file format of a given file, for text file it is easy but for binary it's not obvious: picture, movie, compressed file, ...
use the command file to display what the file format.

.. code-block::

    file foo
    foo: Zip archive data, at least v2.0 to extract, compression method=deflate

    file bar
    bar: gzip compressed data, was "bar", last modified: Fri Feb 12 11:12:38 2016, from Unix, original size modulo 2^32 7866

    file fuzz
    fuzz: PNG image data, 1 x 1, 8-bit/color RGBA, non-interlaced

    file foo_bar
    foo_bar: SVG Scalable Vector Graphics image

    file  ex1.sam
    ex1.sam: Sequence Alignment/Map (SAM), with header version 1.0

Of course the algorithm is not based on the extension. Remind that the extension is just a convention.
