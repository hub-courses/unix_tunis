.. Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

.. role:: red

.. _get_file_from_web:

=================
Get file from web
=================


wget
====

`wget` is a command line utility to retrieve content from web servers.

.. code-block:: shell

   wget http://www.uniprot.org/uniprot/ABCD1_MOUSE.fasta

* wget also supports many different options, such as ftp, https, authentication, etc.


curl
====

`curl` do the same stuff than wget but with another syntax.

By default curl writes the content on standard output (screen). To write the content
to a file add the option `-o, --output <file>`

.. code-block:: shell

    curl -o <file name> <url>


