.. Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

.. role:: red

.. _permissions:

======================
Rights and Permissions
======================

Permissions
===========

* Unix is a multi-users system. So some users have some rights to do something and some other don't.
* The system allows to protect your data from another user.
* The system allows some users to work in groups.

All this features are managed by the **permissions** (or right access)

3 classes of permissions
========================

::

   $ ls -l
   total 1176
   drwxr-xr-x 4 bneron CIB   4096 Mar  4  2014 Alignment
   -rw-r--r-- 1 bneron CIB 220196 Mar  4  2014 CIP-55-138_A.b_A06_037.ab1
   drwxr-xr-x 3 bneron bneron   4096 Jun  4 14:51 HMM

.. figure:: ../_static/unix/images/permissions.png
   :class: align-center
   :width: 650px

| **U**: user
| **G**: group
| **O**: others
| **r**: read      (4)
| **w**: write     (2)
| **x**: execute   (1)


rwx what does it really mean?
=============================

+---+---+------------------------+-------------------------------------------------+
|   |   | file                   | directory                                       |
+===+===+========================+=================================================+
| r | 4 | read                   | allow to list                                   |
+---+---+------------------------+-------------------------------------------------+
| w | 2 | write                  | allow to create and delete file and directories |
+---+---+------------------------+-------------------------------------------------+
| x | 1 | the file is executable | allow to enter and pass through the directory   |
+---+---+------------------------+-------------------------------------------------+

who am I?
=========

Permissions are based on your identity and the groups to which you belong.

* **whoami** => give you the login you logged with
* **id  <login>** => give the id and the groups to which the login belongs (login is optional) ::

   bneron@bic-t2a:~$id
   uid=2896(bneron) gid=3044(CIB) groups=3044(CIB),87(biok),110(sis),547(mobyle),990(gensoft),3160(vpn-ssl-users),20000(bioweb)

* uid is my login
* gid is my primary group
* groups is the list of all groups to which I belong (max 16 groups).


change permissions
==================

**chmod [ugo] [+-] [rwx] <target>**

::

   chmod [ugo] [+-] [rwx] <target>

   drwxr-xr-x 4 bneron CIB   4096 Mar  4  2014 Alignment
   chmod g+w Alignment
   drwxrwxr-x 4 bneron CIB   4096 Mar  4  2014 Alignment
   chmod o-rx  Alignment
   drwxrwx--- 4 bneron CIB   4096 Mar  4  2014 Alignment
   chmod g-wo+x Alignment
   chmod: invalid mode: 'g-wo+x'
   chmod go +rx Alignment
   drwxrwxr-x 4 bneron CIB   4096 Mar  4  2014 Alignment

you can use -R option to apply permission to a directory and recursively to its contents.

change permissions syntax 2
===========================

**chmod num_value <target>**

| num value is composed of 3 digits respectively for
| user, group, other
| each value is the sum of the octal symbols
| for instance for w => 4, read rw => 4+2 = 6 ...

::

   -rw-r--r-- 1 bneron sis 0 Oct 16 13:59 file1
   chmod 755 file1
   -rwxr-xr-x 1 bneron sis 0 Oct 16 13:59 file1
   chmod 640 file1
   -rw-r----- 1 bneron sis 0 Oct 16 13:59 file1


octal notation
==============

+----------------------+----------+-----------------------+
| Symbolic             | Octal    | English               |
| Notation             | Notation |                       |
+======================+==========+=======================+
| \-\-\-\-\-\-\-\-\-\- | 000      | no permissions        |
+----------------------+----------+-----------------------+
| ---x--x--x           | 111      | execute               |
+----------------------+----------+-----------------------+
| --w--w--w-           | 222      | write                 |
+----------------------+----------+-----------------------+
| --wx-wx-wx           | 333      | write & execute       |
+----------------------+----------+-----------------------+
| -r--r--r--           | 444      | read                  |
+----------------------+----------+-----------------------+
| -r-xr-xr-x           | 555      | read & execute        |
+----------------------+----------+-----------------------+
| -rw-rw-rw-           | 666      | read & write          |
+----------------------+----------+-----------------------+
| -rwxrwxrwx           | 777      | read, write & execute |
+----------------------+----------+-----------------------+

**exercises:**

| create files and directories,
| change their permissions.

permissions by default
======================

To control the file/dir's permissions at their creation.

* **umask** display the mask in octal notation: 0022
* **umask -S** display the mask in symbolic notation: u=rwx,g=rx,o=rx

| umask is the complement to 7 to the permissions applied
| umask = 0022

| **directories**: permissions are 777
| 7-0=\ **7**, 7-0=\ **7**, 7-2=\ **5**, 7-2=\ **5**
| directories will be created with 755 rights (rwxr-xr-x)

| **files** is same as dir except, never **x** by default
| 7-0=\ **7**, 7-0=\ **7**, 7-2=\ **5**, 7-2=\ **5** then remove **x** if necessary
| files will be created with 644 rights (rw-r--r--)


octal codes meaning
===================

+----------------+---------------------+----------------+
| Octal digit in |                     |                |
| umask command  | Allows on directory | allows on file |
+================+=====================+================+
| 0              | read, write,execute | read, write    |
+----------------+---------------------+----------------+
| 1              | read and write      | read, write    |
+----------------+---------------------+----------------+
| 2              | read and execute    | read           |
+----------------+---------------------+----------------+
| 3              | read only           | read           |
+----------------+---------------------+----------------+
| 4              | write and execute   | write          |
+----------------+---------------------+----------------+
| 5              | write only          | write          |
+----------------+---------------------+----------------+
| 6              | execute only        | no permissions |
+----------------+---------------------+----------------+
| 7              | no permissions      | no permissions |
+----------------+---------------------+----------------+


setting the umask
=================

* **umask octal_value**
* **umask -S symbolic value**

::

   umask 001
   umask -S
   u=rwx,g=rx,o=rx
   umask -S o+w
   u=rwx,g=rx,o=rwx

exercise:

| create dir and files (use touch) with different umask
| check their permissions with ``ls -l``

.. role:: red

:red:`To set x on file you need to use chmod.`

change owner/group
==================

chown command (for **ch**\ ange **ow** ner)

* **chown owner file/dir**
* **chown owner.group file/dir**

:red:`the ownership of a file may only be altered by a super-user`

* **chgrp group file/dir**

.. warning::

   * Only a member of a group can change a file's group
   * And only to one of which they are a member.

* **-R** option recurse through subdirectories.
