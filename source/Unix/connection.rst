.. Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

.. role:: red

.. _connection:

===========
Connection
===========


How to connect to the central cluster at the Institut.
======================================================

Secure Shell (SSH) is a cryptographic network protocol for secure data communication.

* your **login** and **password** are your identity card and must remain secret.
* ssh is **THE** method to use to connect to a remote computer.
* if your login on the remote computer is different from the one in your current computer
    * ssh remote_server -l login_name
    * ssh login_name@remote_server

* on Mac osX ssh is available.
* on windows install and use **putty**

exercise:

* connect to tars.pasteur.fr
* how to work on central from your "home sweet home" ?


Network accessibility at Pasteur.
=================================

.. figure:: ../_static/unix/images/ip_fortress.png
  :class: align-center
  :width: 500px


Remote computer available at Pasteur
====================================

  * **ssh.pasteur.fr**  (can be accessed from everywhere)
      * public access: YES
      * may run job: NO
  * **maestro.pasteur.fr**
      * public access: NO, must be accessed from Pasteur campus only
      * may run job: YES
      * cluster: YES
      * **need qualification:** `<https://moocs.pasteur.fr/courses/Institut_Pasteur/DSI_01/1/about>`_

  * All central computers on the campus use the same login and password (the one used for your mail)


Put data from your PC to remote server
======================================


* in order to send data from your PC to the remote server, you must use *scp* (*scputty* on windows)

* **scp local_file login@remote_server:remote_path**
* **scp -r local_dir login@remote_server:remote_path**


get data from server to your PC
===============================

* in order to get data from remote server to your PC, you must use *scp* (*scputty* on windows)

* **scp login@remote_server:remote_path local_path**
* **scp -r login@remote_server:remote_path local_path**

