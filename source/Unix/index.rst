.. Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

Unix
====

.. toctree::
    :maxdepth: 2

    file_system
    permissions
    display_files
    chain_commands
    jokers
    finding_files
    search_filter
    archive_and_compressed_files
    get_file_from_web
    transform
    backgrounding_tasks
    scripting
    connection

