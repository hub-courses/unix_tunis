.. _Scripting:

.. role:: red

*********
Scripting
*********

make shortcut command
=====================

* We can create a shortcut for a command we repeat.
* A shortcut for a command is an *alias*
* ``alias shortcut='command'`` 
  (no space surrounding the =)
* ``hgrep='history|grep'``

exercise:

* create a shortcut for a command which extract the 
  bank:entryname from a blast report


make modification permanent
===========================

| Each time we modify the environment this modification
| is available only for the current terminal.
| To turn it permanent just put it in your .bashrc 

*.bashrc* is a file which is executed each time a terminal is created.

exercise 

#. use vim to edit the *~/.profile* to add the following lines. this way when you log again, your *~/.bashrc* will be taken in account

syntax ::

    # use the bashrc
    source ${HOME}/.bashrc

#. make a copy of the *.bashrc* file
#. use vim to edit the *.bashrc* file and add the alias.

in order to test your modifications, do the following command ``source ~/.bashrc``


repeat an action
================

| Sometimes we need to repeat the same action *n* times with just a different detail.
| For instance rename all files in a directory.
| For this we use a loop:

syntax:

.. code-block:: shell
 
    for var in list items
    do
        do something
    done

to be useful we need :

* to set variable ``var=value`` (NO spaces surrounding =)
* to read variable ``$var``

example:
--------

remove '.fa' from all files terminating by 'fa.gde'
 
.. code-block:: shell
 
   for f in `ls il2*.fa.gde`
   do
      nam=`basename $f .fa.gde`
      mv $f $nam.gde
   done

exercise:
---------

#. copy the Proteique directory from the unix_training projets in **central-bio** in your **local** home
#. reformat all fasta file (*.fa* file) using squizz in *gde* format.

scripts
-------

* scripts are executables text files that contains:
* the instruction to execute.
* the flow control instructions.
* a ``sheebang`` that describes the interpreter to use, eg: ``#! /bin/bash``. the ``sheebang`` must be the first line of the script.

scripts arguments
-----------------

* ``$0`` name of the script
* ``$1, $2 ... $n`` positional arguments received by the script
* ``$#`` number of positional arguments received
* ``$@`` list of arguments


tests
-----

you will find 2 syntax for the test

* ``[ expression ]``
* ``test expression``

expression evaluate in true (0) or false (1)

syntax: ::

    var="hello"
    [ $var = "hello" ]
    echo $? 

==> 0 means true

syntax:

.. code-block:: shell

    var=3
    [ $var -eq 2 ]
    echo $? 

==> 1 means false

put test at work
----------------

syntax:

.. code-block:: shell

    #! /bin/sh

    # check if we got arguments
    if [ $# -eq  0 ] 
      then 
        echo "no argument provided: exit"
        exit 1
    fi

    # check if file exists
    if [ -f $1 ]
      then
        echo "file $1 exists"
        ret=0
      else
        echo "file $1 does not exists"
        ret=1
    fi
    
    # exit with significative return value
    exit $ret

