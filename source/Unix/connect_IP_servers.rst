.. _Connect_IP_servers:

************************************
How and where to connect IP servers?
************************************


servers at pasteur
==================

 
.. figure:: ../_static/unix/images/ssh-pasteur-fr.png
   :class: align-center
   :width: 700px
   
how to connect to a remote server
=================================

Secure Shell (SSH) is a cryptographic network protocol for secure data communication.

* ssh remote_server -l login_name
* ssh login_name@remote_server

* on Mac osX ssh is available.
* on windows use **putty** **scputty**

where are your homes
====================

.. figure:: ../_static/unix/images/home_abs_path.png
   :class: align-center
   :width: 600px

where are your unites
=====================

.. figure:: ../_static/unix/images/unites_abs_path.png
   :class: align-center
   :width: 600px

where are your project
======================

.. figure:: ../_static/unix/images/projets_abs_path.png
   :class: align-center
   :width: 600px


relative path
=============

.. figure:: ../_static/unix/images/relative_path.png
   :class: align-center
   :width: 600px

 

put data from your PC to server
===============================


* put your data on you PC in @home => data will appear in your home dir.
* or your data on @unit_name  => data will appear in your unit dir.
  (this is the favorite method)
  
or

* **scp local_file login@remote_server:remote_path**
* **scp -r local_dir login@remote_server:remote_path**

get data from server to your PC
===============================


* put the the data of your home dir or unit dir
  they will appear on your PC in @home or @unit_name volume.
* some projects are also accessible. 

or
 
| On Unix you can have a ssh server.
| *activate it on mac osX*
| the remote server become your PC.

why it's better to use @home/@unit
==================================

.. figure:: ../_static/unix/images/ssh-pasteur-fr.png
   :class: align-center
   :width: 700px