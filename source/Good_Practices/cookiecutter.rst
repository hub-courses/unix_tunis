.. Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

.. role:: red

.. _cookiecutter:

======================
Good project structure
======================

.. image:: ../_static/good_practices/cookiecutter_project_structure.png
    :alt: project structure
    :align: right


Adopt well structured project.

The structure of a project depends on the topic, language on so on.

So there is lot of available structures, but choose one which is largely adopted by the community.
It facilitates the communication with the collaborators.
It will also ease the packaging, sharing, and the reproducibility, at the end.


.. container:: clearer

    .. image :: ../_static/imgs/spacer.png


Cookiecutter
============


`cookie cutter <https://github.com/cookiecutter/cookiecutter>`_ is a command-line utility that creates projects
from cookiecutters (project templates), e.g. creating a Python package project from a Python package project template.
The project start by providing template for python project but now it exists thousands of templates
for all languages and topics.

.. image:: ../_static/good_practices/cookiecutter_github.png
    :alt: cookiecutter

Have a look on the templates shared by the community: https://github.com/topics/cookiecutter-template

To create a project you have to use a template, a lot of these templates are available on github.
Cookiecutter can directly use template hosted on github

:code:`python3 -m cookiecutter <github url of template>`

And answer to the question to customize your project.

Finally, if you do not find what you need in these templates, create your template from an existing one.

It could be a good idea, especially when you work on a platform, to have a set of templates for
some typical tasks. It helps to standardize the analysis.


Exercise
========


#. install cookiecutter with `apt`
#. go in your home directory
#. create a project using `cookiecutter` and the template https://github.com/LisaHagenau/cookiecutter-template
    * name of the project `cdc_cov2`
#. Explore your project tree files



