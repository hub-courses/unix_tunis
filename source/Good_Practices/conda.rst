.. Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

.. role:: red

.. _conda:

===============================
Reproducible environment: Conda
===============================

Installation
============

From the source
---------------

First we will install **miniconda**, which is a light version of conda.
Miniconda will create a conda environment with the **conda** command line interface (CLI).

#. Go to the download page https://docs.conda.io/en/latest/miniconda.html.
#. Download the relevant installer file (see notes here below).
#. Execute the downloaded installer (script shell).


For Linux users, you should see a section like this one:

.. image:: ../_static/good_practices/conda_miniconda.png

.. note::

    What about the Python versions and 32/64 bits flavours ?

    #. You have the choice between different architectures (e.g. 32/64 bits). Usually, with
       modern architectures, the 64bits version is the one to download.
    #. You have the choice between several version of Python (e.g., 2.7, 3.7, etc).
       This version is independent of the version installed on your computer.
       This will be the version used by conda internally in the first environment (the **base**).
       Later on you may create environments with other Python version anyway. 
       So, you may choose the latest one (e.g. python 3.9).

As an example, in a Linux terminal you can download this version::

    cd Downloads
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

and then execute the installer::

    sh Miniconda3-latest-Linux-x86_64.sh

Follow the instructions. Answer yes to "Do you wish the installer to initialize Miniconda3 by running conda init ?"

The installation will create a miniconda folder, by default in the home directory ``(~/miniconda3)``.

Say **no** when `conda` ask you to activate conda by default

.. note::

    You always change this behavior by running the command

    :code:`conda config --set auto_activate_base true`

    or

    :code:`conda config --set auto_activate_base false`


4. Exit the shell and start a new one for the changes to be active.
5. Update conda::

     conda update conda


From debian package
-------------------

https://docs.conda.io/projects/conda/en/latest/user-guide/install/rpm-debian.html

#. Update the list of packages
    .. code-block:: shell

        $ sudo apt-get update -y

#. Install prerequisites
    .. code-block:: shell

        $ sudo apt-get install -y wget curl gpg

#. Install our public GPG key to trusted store
    .. code-block:: shell

        $ sudo curl https://repo.anaconda.com/pkgs/misc/gpgkeys/anaconda.asc | gpg --dearmor > conda.gpg
        $ sudo install -o root -g root -m 644 conda.gpg /usr/share/keyrings/conda-archive-keyring.gpg

#. Check whether fingerprint is correct (will output an error message otherwise)
    .. code-block:: shell

        gpg --keyring /usr/share/keyrings/conda-archive-keyring.gpg --no-default-keyring --fingerprint 34161F5BF5EB1D4BFBBB8F0A8AEB4F8B29D82806

#. Add our Debian repo
    .. code-block:: shell

        echo "deb [arch=amd64 signed-by=/usr/share/keyrings/conda-archive-keyring.gpg] https://repo.anaconda.com/pkgs/misc/debrepo/conda stable main" > /etc/apt/sources.list.d/conda.list

    **NB:** If you receive a Permission denied error when trying to run the above command
    (because `/etc/apt/sources.list.d/conda.list` is write protected), try using the following command instead:

    .. code-block:: shell

        echo "deb [arch=amd64 signed-by=/usr/share/keyrings/conda-archive-keyring.gpg] https://repo.anaconda.com/pkgs/misc/debrepo/conda stable main" | sudo tee -a /etc/apt/sources.list.d/conda.list

#. Update the list of packages
    .. code-block:: shell

        $ sudo apt-get update -y

#. Install conda
    .. code-block:: shell

        $ sudo apt-get install conda

#. check the installation
    .. code-block:: shell

        $ source /opt/conda/etc/profile.d/conda.sh
        $ conda -V
        conda 4.12.0


Installing a Conda package
==========================

#. First activate conda

activate conda

.. note::

    The (base) at the beginning of your prompt indicates that you are in the base environment.
    We will see environments later on.

We will first install a standard Python package called **pandas**::

    conda install pandas

It should be installed in a few seconds with a good internet connection!
Installing it normally would have taken a little more time and compilation would
have been required.

Now what about a bioinformatics tools such as bwa ? ::

    conda install bwa

You should get an error (unknown package). 

Let us see how to configure conda to be able to install more specific tools.


Configuring channels
====================

One strength of the **Conda** community is that dedicated **channels** have been
created for different scientific communities. For instance, BioConda
(https://bioconda.github.io/) for bioinformatics.


To benefit from those channels, we need to globally configure the sources of the packages to install.
We will add new sources to the ``defaults`` channel already configured in the original installation.

First let's check the currently configured channels with ``conda config``::

  conda config --show channels

This information is actually stored in the file ``~/.condarc``.

Now, we add more channels::

  conda config --add channels bioconda
  conda config --add channels conda-forge

You can now verify that your channel are properly configured with::

  conda config --show channels

.. warning::

    Channels are appearing in **order of priority**, which depends on the order in which you specified the
    ``conda config --add channels``. By default, to identify the package which will be installed,
    conda sorts packages from highest to lowest channel priority, then by version number, then by build number.
    This means that if you try to install ``pandas`` for example, conda will look first in conda-forge for the
    most recent version and builds. If it is not present in conda-forge, then it will look for it in bioconda
    and then in defaults.

Updating a package
==================

You can now update **pandas** from the conda-forge or bioconda channel (instead of the ``defaults`` source as before)::

    conda update pandas

Creating an environment
=======================

So far we used the main **base** environment but one can create as many
environments as desired. A good practice is to keep the **base** clean and
functional. 

So, let us create a new environment, which will encapsulate the different softwares we will need for our analysis.

This is done with ``conda create``::

  conda create --name repro python=3.7
  # Equivalent to
  conda create -n repro python=3.7


Here we create a new environment called **repro** and we install python
version 3.7. We used the fuzzy match ``=`` sign so actually, at the time it will be python 3.7.12 which will be installed.
If you want to be more precise on the version specification you can use ``python==3.7.12`` instead.

We can then list different environments that have been created so far

.. code-block::

    $ conda info --envs
    # conda environments:
    #
    base                   *  /home/cronos/miniconda3

The currently active environment is highlighted by a ``*``. You can see as well the absolute location of the environment folder.

Creating an environment from an environment.yml file
----------------------------------------------------

.. code-block::

    $ conda env create --file environment.yml



Activate/Deactivate an environment
==================================

Depending on the configuration, when you start a new shell, the active
environment is the **base** environment. You can deactivate it totally using::

    conda deactivate

Then, you can activate another environment::

    conda activate repro

Activate the base and/or repro environments and check that the changes in python
version using ``python --version``.


Installing packages in the environment
======================================

First we will activate the environment::

  conda activate repro

You can see that the information of your current environment is displayed in the bash prompt (ie ``(repro)``).

And install in it some packages of interest::

  conda install bwa samtools ivar pangolin nextclade_js

You can check the packages installed, their version, build and source in the current environment using::

  conda list

.. note::

    Environment creation and package installations can be done in a single command,
    for example::

	    conda create --name myenv python==3.7.12 bwa samtools ivar pangolin nextclade_js

Searching packages in channels
==============================

You can search for a particular package in the channels you defined like so::

  conda search deseq

The results will give you all the packages fitting the ``*deseq*`` pattern
as well as all the versions available in the different channels.

	    
Exporting/Reproducing environments
==================================

One way to reach a certain degree of reproducibility is to share your environment with the community.
You can do so by exporting the content of your **active** environment using::

  conda env export > repro.yml

Your channel setup and packages with version and builds will be then stored in the YAML file ``repro.yml``.

You could now delete this environment and reproduce it from the YAML file::

  # You will need to deactivate the environment before removing
  conda deactivate
  conda env remove -n repro
  # And reproducing it from the yaml
  conda env create --file repro.yml
  # If you want to use another environment name than the one specified in the YAML
  conda env create -n repro_copy --file repro.yml

.. note::

    By experience, incorporating the builds in the environment export can lead to bugs for environment reproduction.
    You can use this command as a workaround::

        conda env export --no-builds > repro_no_builds.yml


Mamba
=====

If you find conda install:

* too slow
* consuming too much memory
* to not be able to resolve dependencies either with anaconda or miniconda

Try `mamba <https://mamba.readthedocs.io/en/latest/user_guide/mamba.html>`_. In your **base** environment

.. code-block::

    conda install mamba -n base -c conda-forge

This command will install mamba in the *base* environment (`-n`)
using the latest version of mamba package found in *conda-forge* channel (`-c`)

`mamba` is a drop-in replacement and uses the same commands and configuration options as `conda`.
You can swap almost all commands between conda & mamba

.. code-block::

    mamba install ...
    mamba create -n ... -c ... ...
    mamba list

For instance to create an environment from a conda environment file.yml

.. code-block::

    mamba env create --file environment.yml

.. warning::

    The only difference is that you should still use `conda` for activation and deactivation of environments.


Exercises
=========

We will continue to setup our environment for all the week

#. :red:`install mamba` in base environment
#. go in cookiecutter made project
#. go in directory `src`
#. import `artic-ncov2019` (https://github.com/artic-network/artic-ncov2019) with *git*
#. replace the environment at the root of cookiecutter project by this provided by artic-ncov2019
#. go to the root of the project
#. edit the environment file
    * change the name of the project to `cdc_cov2`
#. use :red:`**mamba**` to create an environment using the *environment.yml* at the project root directory
#. activate the `cdc_cov2`
#. test if the software `rampart` is present : :code:`rampart -v`
#. deactivate the `cdc_cov2`

**We will use this environment during all the course.**

| If we have to install other bioinformatic tools via conda/mamba think to update the *environment.yml* file.
| It will allow you to reproduce your results, and share this environment with your collaborators.

