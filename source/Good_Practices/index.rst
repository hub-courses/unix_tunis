.. Good Practices for Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis


==============
Good Practices
==============


.. toctree::
    :maxdepth: 2

    cookiecutter
    conda
    containers