.. Linux Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

==================
Linux Installation
==================

.. toctree::
    :maxdepth: 1

    factory_settings
    windows
    linux_install
    linux_admin

