.. Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

.. _factory_settings:

================
Factory Settings
================

The factory settings do not allow us to install Linux alongside windows.

Windows use the whole disk:

+-----------+----------------+--------+
| partition |       type     |  size  |
+===========+================+========+
|    1      | SYSTEM         | 260 Mo |
+-----------+----------------+--------+
|    2      | MSR (reserved) | 16 Mo  |
+-----------+----------------+--------+
|    3      | Principal      | 476 Go |
+-----------+----------------+--------+
|    4      | rescue         | 116 Mo |
+-----------+----------------+--------+
|    5      |                | 22 Go  |
+-----------+----------------+--------+
|    6      | MYASUS         | 200 Mo |
+-----------+----------------+--------+

Our objective is to install windows on a 200Go partition and linux on a 275 Go partition. So we will
merge the partion 3, 4, 5 and split this new space into two. Beware this operation will destroy
anything on the disk.

Windows needs ms cloud identifier, the partition table is locked.

So we need to reinstall windows (windows 10) and use part of the disk for Windows and part for
Linux.

