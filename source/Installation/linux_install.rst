 .. Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

.. role:: red

.. _linux_install:

==================
Linux Installation
==================


Which distros
=============

Linux belongs the Unix family operating systems.
But it exists lots of linux flavors called distributions.
Some of them are free, some are commercials.
Some are widely used some are more confidentials,...

https://en.wikipedia.org/wiki/List_of_Linux_distributions

https://upload.wikimedia.org/wikipedia/commons/b/b5/Linux_Distribution_Timeline_21_10_2021.svg

We choose Xubuntu. Xubuntu is Ubuntu with Xfce as window manager.  Ubuntu is very widely used and
based on debian so it's well maintained with a lot of packages. The Xfce window manager is
lighweight and close to classical window managers.


Installation
=============

Create a bootable usbkey
------------------------

We prepare a usbkey to boot on and install xubuntu.

To create a bootable usbkey with Xubuntu:

#. go to the download page
#. download the image (iso file)
#. from any unix operating system, create the key with:
    #. check the device of the key (do not mount it)
       usually /dev/sd? ? match the name of device (a,b,...) it depends of the sd device already mounted

    #. copy the isofile on the key
        .. code-block::

            dd if=/path/to/iso_file.iso of=/dev/sda

        This operation copies the isofile and reformats the key. So everything on the key is deleted. Therefore be sure of the correct "sd" name of your device before running this command !


Install Xubuntu
---------------

Boot on usbkey
""""""""""""""

Start the computer and press the `escape` key.
You will access the boot menu.

Choose the usbkey


.. image:: ../_static/install/linux_boot_key.jpg

Select Try or install Xubuntu
"""""""""""""""""""""""""""""

.. image:: ../_static/install/linux_install_xubuntu.jpg


Select the Operating System language
""""""""""""""""""""""""""""""""""""

I advise to choose English because it will be easier to debug potential some error messages.  To
resolve the problem, you will only have to copy/paste the error message in Google and see what
solutions come up. If you choose another language, you will have fewer to no help depending on the
language. Whereas if you choose English, very often the problem has been already solved by someone and
you'll find help easily.

Then click on Install Xubuntu

.. image:: ../_static/install/linux_select_lang.jpg


Select the keyboard layout
""""""""""""""""""""""""""


.. image:: ../_static/install/linux_select_keyboard.jpg


Choose the installation mode
""""""""""""""""""""""""""""
We will use:
* normal installation mode
* with download the update  (available only if you are connected to internet)
* and third party drivers installation (available only if you are connected to internet)

.. image:: ../_static/install/linux_select_install_mode.jpg


Dual boot
"""""""""

Xubuntu will detect that windows is already installed, and there is a not allocated partition.
Choose to install Xubuntu alongside windows if you want dual boot

If you want only linux, choose: "Erase disk and install Xubuntu"


.. image:: ../_static/install/linux_install_dual_boot.jpg


.. warning::

    This operation cannot be reverted. If you change your mind you will have to start again from the beginning.


Create main user
""""""""""""""""

.. image:: ../_static/install/linux_install_dual_boot.jpg

Choose a good password

* 8 characters at least
* contains lower/upper case
* contains digit
* contains special characters  !@#$%^&*()_-+=,.;/~

It's very important especially if you are connected to the net (ssh, ...)

Install proprietary drivers for the graphics card
-------------------------------------------------

Your graphics card is a NVIDIA. This company provides non free drivers to get better performances.
By default the installed drivers is `nouveau` which is open source.
It works well but some bioinformatics software use the graphic card on gpu which offer better performance.
So we can installed the NVIDIA drivers

Update system
"""""""""""""
Before to start the installation phase, we will update the system to get le last version of the drivers

.. code-block::

    sudo apt update
    sudo apt upgrade -y

Detection of cards and drivers
""""""""""""""""""""""""""""""

Ubuntu propose a command to auto detect our cards and propose some compatible drivers

.. code-block::
    :emphasize-lines: 8

    $ ubuntu-drivers autoinstall
    == /sys/devices/pci0000:00/0000:00:01.1/0000:01:00.0 ==
    modalias : pci:v000010DEd0000249Csv00001043sd000013DDbc03sc00i00
    vendor   : NVIDIA Corporation
    model    : GA104M [GeForce RTX 3080 Mobile / Max-Q 8GB/16GB]
    driver   : nvidia-driver-470-server - distro non-free
    driver   : nvidia-driver-510 - distro non-free
    driver   : nvidia-driver-515 - distro non-free recommended
    driver   : nvidia-driver-470 - distro non-free
    driver   : nvidia-driver-515-server - distro non-free
    driver   : nvidia-driver-510-server - distro non-free
    driver   : xserver-xorg-video-nouveau - distro free builtin

Here the system propose to install driver-515 (see `recommended` on higlighted line)
We are agree with this choice.

Installation of the driver
""""""""""""""""""""""""""

To install the driver proposed by the detection step

.. code-block::

    $ sudo ubuntu-drivers autoinstall

Check if nvidia works
"""""""""""""""""""""

.. code-block::

    nvidia-smi
    Thu Sep  8 11:24:06 2022
    +-----------------------------------------------------------------------------+
    | NVIDIA-SMI 515.65.01    Driver Version: 515.65.01    CUDA Version: 11.7     |
    |-------------------------------+----------------------+----------------------+
    | GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
    | Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
    |                               |                      |               MIG M. |
    |===============================+======================+======================|
    |   0  NVIDIA GeForce ...  Off  | 00000000:01:00.0 Off |                  N/A |
    | N/A   33C    P3    N/A /  N/A |      5MiB /  8192MiB |      0%      Default |
    |                               |                      |                  N/A |
    +-------------------------------+----------------------+----------------------+

    +-----------------------------------------------------------------------------+
    | Processes:                                                                  |
    |  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
    |        ID   ID                                                   Usage      |
    |=============================================================================|
    |    0   N/A  N/A      1104      G   /usr/lib/xorg/Xorg                  4MiB |
    +-----------------------------------------------------------------------------+

(source: https://www.linuxcapable.com/how-to-install-nvidia-drivers-on-ubuntu-22-04-lts/)

Administration
==============

Set the date and time
=====================

Sudoers
=======

In most of distros, there is a special user named `root` which have the rights to do anything.  This
role is dangerous so to avoid mistakes we reserve it to special actions and take care of what you do
when you are logged as root.

Debian/Ubuntu have no root user anymore, but some users are granted for some special operations that need root privileges.
The first user you created during the installation phase is a sudoer and is granted for all root privileges for all operations.
To be granted and execute a command with the root privileges, you need to precede your command by the keyword "sudo" (which means 'super user do')
The first time you execute a command as sudo you will be asked to confirm your password.
Your password is available for one shell, so if you execute a second command in an other terminal you will be asked again for your password.

.. code-block::

    sudo useradd ...


Create new user
===============

.. code-block:: shell

    sudo useradd  -u UID -g <primary group>  -G <group1,group2,...>  -m   --shell /bin/bash

Only root can perform this command.

Create new group
================

.. code-block:: shell

    sudo groupadd <name of group>

Only root can perform this command.

To take into account a new user and group you'll need to logout and login again.

Check you identity
==================

whoami
------

id
--

Install package
===============

Linux distinguishes itself from other operating system by packaging a lot of software and a package management system.
Each linux distros have it's own packaging system:

* centos/redhat have yum/dnf package management system and .rpm packages
* debian/ubuntu have apt package management system with .deb packages
* gentoo have portage package management system with .ebuild packages


The advantages of official packages is that the installation of the software is easy and well tested.
The maintenance of these packages is ensured by the packages manager.
A lot of bioinformatics software are packages in these distros.

The inconvenient of these packages is that often the latest bleeding edge version of software is not
available.  The package manager can handle only one version of each package at the time (for
instance you cannot use blast+2.11 for one project and 2.0 for an other one). The packages are
upgraded when you upgrade the system, so the version can change during a project life time. We will
see alternatives later on.

Search
------

To search if a software is packaged for your ubuntu use the apt command

.. code-block::

    apt search <soft name>

The low level command, to use if it's inside a script (dockerfile, ...)

.. code-block::

    apt-cache search <soft name>


Install
-------

To install a package, once you have the exact name of the package you want to install

.. code-block::

   sudo apt install <package name>

.. code-block::

    sudo apt-get install <package name>


    .. note::

        Notice that anybody can search if a package exists, but only granted users can install a new package.


Update system
=============

To update the whole system (all packages), it can be performed via a widget "software updater",
but I will describe below the command line way.

You need to update the list of package, then perform the upgrade of the packages

.. code-block::

    sudo apt update
    sudo apt upgrade

.. code-block::

    sudo apt-get update
    sudo apt-get upgrade

Debian/Ubuntu are distros based on releases, sometimes you need to switch to a new release.
In order to do so, you need to perform a `distupgrade` but only when a new release is available.
You will be warned for new releases by the package manager after a "classical" update operation.

.. code-block::

    sudo apt-get distupgrade

