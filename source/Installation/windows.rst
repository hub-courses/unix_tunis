.. Pasteur Network Course: Bioinformatics for SARS-CoV-2 sequence analysis

.. role:: red

.. _windows:

=======
Windows
=======

Boot on windows 10 key
======================

Turn on the computer and press `escape` to enter the boot menu

.. image:: ../_static/install/windows_boot_key.jpg


Select windows lang
===================

.. image:: ../_static/install/linux_select_lang.jpg


Install windows
===============

.. image:: ../_static/install/windows_install.jpg

Accept license
==============

.. image:: ../_static/install/windows_license.jpg


Do a custom installation
========================

.. image:: ../_static/install/windows_custom_install.jpg

Rearange partition table
========================

.. image:: ../_static/install/windows_rearange_partition.jpg

#. select partition 3 and "supprimer"
#. select partition 4 and "supprimer"
#. select partition 5 and "supprimer"

So we now have 476.5Go not allocated partition, that we split into two

.. image:: ../_static/install/windows_new_partition.jpg


Select the not allocated space, click on `new` then set the size 204800 Mo (200 Go * 1024).

Windows will be install on this partition.
When we will install linux it will detect the not allocated partition and propose to install on it.


